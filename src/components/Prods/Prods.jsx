import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import "./_prods.scss";
import Product from "../Product/Product";
import Button from "../UI/Button/Button";
import { productActions } from "../Store/product";

export default function Prods({ ogInfo, category }) {
  const dispatch = useDispatch();
  const { prodsImg } = useSelector((state) => state.product);
  const { name: categoryName, title: categoryTitle } = category;

  // View more product handle
  const navigate = useNavigate();
  const viewMoreProdsByCategory = () => {
    dispatch(productActions.setCategory(categoryName));
    navigate("/san-pham");
  };

  return (
    <div className="Prods">
      <div className="Prods__title">{categoryTitle}</div>
      <div className="Prods__products">
        {ogInfo &&
          prodsImg &&
          ogInfo[categoryName].map((prodInfo) => (
            <Product
              key={prodInfo.id}
              prodPath={"san-pham/" + prodInfo.id}
              prodId={prodInfo.id}
              prodTitle={prodInfo.title}
              prodPrices={prodInfo.prices}
              prodImg={prodsImg.find((prodImg) => prodImg[prodInfo.id])}
            />
          ))}
      </div>
      <Button
        className="Prods__btn"
        borderType
        onClick={viewMoreProdsByCategory}
      >
        Xem thêm
      </Button>
    </div>
  );
}
