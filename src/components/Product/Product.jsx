import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

import "./_product.scss";
import SizeDropdown from "./SizeDropdown";
import { showToast } from "../UI/Helper/ToastMessage";

export default function Product({
  className,
  prodTitle,
  prodPrices,
  prodPath,
  prodId,
  prodImg,
}) {
  // Handle image fetching

  const [selectedPrice, setSelectedPrice] = useState(null);
  // First render will display the smallest size & price
  useEffect(() => {
    setSelectedPrice(prodPrices[0]);
  }, [prodPrices]);
  // Handle selected price and size
  const priceSizeSelect = (selected) => {
    setSelectedPrice(selected);
  };
  // Handle navigate to product page
  const nav = useNavigate();

  // Add to cart
  const addToCart = (event) => {
    event.stopPropagation();
    showToast();
  };

  return (
    <div className={`Product ${className || ""}`} onClick={() => nav(prodPath)}>
      {prodImg && (
        <div className="Product__img">
          <img src={prodImg[prodId][0].url} alt={prodImg[prodId][0].name} />
        </div>
      )}
      <div className="Product__desc">
        <div className="Product__desc-container">
          <div className="Product__desc-title">{prodTitle}</div>
          {selectedPrice && (
            <div className="Product__desc-price">
              <span className="Product__desc-price--current">
                <span>{selectedPrice.current}</span>
                <span className="currency-vnd">₫</span>
              </span>
              {selectedPrice.old && (
                <span className="Product__desc-price--old">
                  <span>{selectedPrice.old}</span>
                  <span className="currency-vnd">₫</span>
                </span>
              )}
            </div>
          )}
          {prodPrices && selectedPrice && (
            <div className="Product__desc-cta">
              <SizeDropdown
                selected={selectedPrice}
                prices={prodPrices}
                onSelected={priceSizeSelect}
              />
              <div className="Product__desc-cta-add" onClick={addToCart}>
                Thêm vào giỏ
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
