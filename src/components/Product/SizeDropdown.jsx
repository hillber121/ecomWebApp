import { useState, useEffect } from "react";
import { Transition } from "react-transition-group";

import "./_sizeDropdown.scss";
import { ArrowDIcon } from "../SvgFile/SvgFile";

export default function SizeDropdown(props) {
  const [isDropdownClose, setIsDropdownClose] = useState(true);
  // If dropdown is opening, clicking outside or selecting other option will close it
  useEffect(() => {
    const closeDropdown = () => {
      setIsDropdownClose(true);
    };
    document.body.addEventListener("click", closeDropdown);
    return () => {
      document.body.removeEventListener("click", closeDropdown);
    };
  }, []);
  // Options data and option select handle
  const sizes = props.prices.map((price) => price.size);
  const selectHandler = (event) => {
    event.stopPropagation();
    if (props.selected.size) {
      const selectedSize = event.target.innerText;
      const selected = props.prices.find(
        (price) => price.size === selectedSize
      );
      props.onSelected(selected);
    }
    setIsDropdownClose(true);
  };
  // Handle show and hide dropdown list
  const toggleDropdown = (event) => {
    event.stopPropagation();
    setIsDropdownClose((prev) => !prev);
  };
  // sizeDropdown classes
  const sizeDropdownClasses = `SizeDropdown ${
    !isDropdownClose ? "SizeDropdown--active" : ""
  }`;
  // Transition configuration
  const duration = 200;
  const defaultStyles = {
    transition: `all ${duration}ms ease-in-out`,
    opacity: 0,
  };
  const slideDownStyles = {
    entering: { opacity: 1 },
    entered: { opacity: 1 },
    exiting: { opacity: 0 },
    exited: { opacity: 0 },
  };

  return (
    <div className={sizeDropdownClasses}>
      <div onClick={toggleDropdown} className="SizeDropdown__default">
        {props.selected.size || "-"}
        {props.selected.size && <ArrowDIcon />}
      </div>
      <Transition
        in={!isDropdownClose}
        timeout={duration}
        mountOnEnter
        unmountOnExit
      >
        {(state) => (
          <div
            className="SizeDropdown__opts"
            style={{
              ...defaultStyles,
              ...slideDownStyles[state],
            }}
          >
            {sizes.map((option) => (
              <div
                key={option}
                onClick={selectHandler}
                className="SizeDropdown__opts-opt"
              >
                {props.selected.size ? option : "-"}
              </div>
            ))}
          </div>
        )}
      </Transition>
    </div>
  );
}
