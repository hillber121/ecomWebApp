import { Link, useLocation } from "react-router-dom";

import "./_navList.scss";

const navBtns = [
  { path: "/", text: "Trang chủ" },
  { path: "cua-hang", text: "Cửa hàng" },
  { path: "dich-vu", text: "Dịch vụ" },
  { path: "gioi-thieu", text: "Giới thiệu" },
  { path: "ho-tro", text: "Hỗ trợ" },
  { path: "san-pham", text: "Sản phẩm" },
];

export default function NavList({ style }) {
  // Detect which page is displaying
  const pathName = useLocation().pathname;

  const navBtnsComp = (
    <ul className="NavList" style={style}>
      {navBtns.map((navBtn) => {
        let navBtnClasses = "";
        // Highlight products nav
        if (navBtn.path === "san-pham") {
          navBtnClasses += "NavList__btn--highlight ";
        }

        if (navBtn.path !== "/") {
          navBtnClasses +=
            pathName.indexOf(navBtn.path) !== -1 ? "NavList__btn--active" : "";
        } else {
          navBtnClasses +=
            pathName === navBtn.path ? "NavList__btn--active" : "";
        }
        return (
          <li key={navBtn.path}>
            <Link className={navBtnClasses} to={navBtn.path}>
              {navBtn.text}
            </Link>
          </li>
        );
      })}
    </ul>
  );

  return navBtnsComp;
}
