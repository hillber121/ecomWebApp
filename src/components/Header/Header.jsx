import { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import "./_header.scss";
import NavList from "./NavList/NavList";
import Input from "../UI/Input/Input";
import Modal from "../Modal/Modal";
import UserSign from "../UserSign/UserSign";
import { ModalActions } from "../Store/modal";
import { SearchIcon, HeartIcon, CartIcon, UserIcon } from "../SvgFile/SvgFile";
import logoImg from "../../assets/images/logo.svg";
import { showToast } from "../UI/Helper/ToastMessage";

export default function Header() {
  // Handle header scrolling
  const [isScrolled, setIsScrolled] = useState(window.scrollY !== 0);
  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY !== 0) {
        setIsScrolled(true);
      } else setIsScrolled(false);
    };
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);
  const headerClasses = `Header ${isScrolled ? `Header--shadow` : ""}`;

  // Modal handle
  const dispatch = useDispatch();
  const isModalOpen = useSelector((state) => state.modal.isModalOpen);
  const openModalHandler = () => {
    dispatch(ModalActions.openModal());
  };
  const closeModalHandler = () => {
    dispatch(ModalActions.closeModal());
  };
  // Change path
  const navigate = useNavigate();
  const heartBtnHandler = () => {
    navigate("/yeuthich");
  };
  const cartBtnHandler = () => {
    navigate("/giohang");
  };
  // Show/Hide search input handle
  const [searching, setSearching] = useState(false);
  const searchShowHandler = (event) => {
    event.stopPropagation();
    setSearching(true);
  };
  // If search field is opening, clicking outside or selecting other option will close it
  useEffect(() => {
    const closeSearchField = () => {
      setSearching(false);
    };
    document.body.addEventListener("click", closeSearchField);
    return () => {
      document.body.removeEventListener("click", closeSearchField);
    };
  }, []);
  // Search input attributes
  const searchInpAtts = {
    id: "searchInp",
    type: "text",
    placeholder: "Tìm kiếm sản phẩm...",
  };
  const searchInpIcon = {
    comp: <SearchIcon />,
    submit: true,
  };
  // Search handle
  const searchFieldClick = (event) => {
    event.stopPropagation();
  };
  const searchHandler = (event) => {
    event.preventDefault();
    showToast();
  };

  return (
    <>
      <header className={headerClasses}>
        <div className="grid">
          <div className="row">
            <div className="Header-container">
              <Link to="/">
                <img className="Header__logo" src={logoImg} alt="logo" />
              </Link>
              {searching ? (
                <form onSubmit={searchHandler} onClick={searchFieldClick}>
                  <Input inp={searchInpAtts} inpIcon={searchInpIcon} />
                </form>
              ) : (
                <NavList />
              )}
              <ul className="Header__left">
                {!searching && (
                  <li onClick={searchShowHandler}>
                    <SearchIcon />
                  </li>
                )}
                <li onClick={heartBtnHandler}>
                  <HeartIcon />
                </li>
                <li onClick={openModalHandler}>
                  <UserIcon />
                </li>
                <li onClick={cartBtnHandler}>
                  <CartIcon />
                  <div className="Header__left-badge"></div>
                </li>
              </ul>
            </div>
          </div>
          {searching && (
            <div className="Header__navList--sub">
              <NavList />
            </div>
          )}
        </div>
      </header>
      {isModalOpen && (
        <Modal onBackdropClick={closeModalHandler}>
          <UserSign />
        </Modal>
      )}
    </>
  );
}
