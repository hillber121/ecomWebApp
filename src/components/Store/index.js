import { configureStore } from "@reduxjs/toolkit";

import modalReducer from "./modal";
import productReducer from "./product";

const store = configureStore({
  reducer: { modal: modalReducer, product: productReducer },
});

export default store;
