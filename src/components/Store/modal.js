import { createSlice } from "@reduxjs/toolkit";

const initialModalState = { isModalOpen: false, loginState: true };

const ModalSlice = createSlice({
  name: "Modal",
  initialState: initialModalState,
  reducers: {
    openModal(state) {
      state.isModalOpen = true;
    },
    closeModal(state) {
      state.isModalOpen = false;
    },
    toSignup(state) {
      state.loginState = false;
    },
    toLogin(state) {
      state.loginState = true;
    },
  },
});

export const ModalActions = ModalSlice.actions;
export default ModalSlice.reducer;
