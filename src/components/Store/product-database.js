import { storage } from "./../Storage/firebaseConfig";
import { ref, listAll, getDownloadURL } from "firebase/storage";

import { productActions } from "./product";

export const fetchProductData = () => {
  return async (dispatch) => {
    const fetchData = async () => {
      const response = await fetch(
        process.env.REACT_APP_PUBLIC_FIREBASE_REALTIME_DATABASE_URL
      );

      if (!response.ok) {
        throw new Error("Could not fetch products data");
      }

      const productData = await response.json();
      return productData;
    };

    try {
      const productData = await fetchData();
      dispatch(productActions.setData(productData));
      return productData;
    } catch (error) {
      throw new Error("Could not fetch products data");
    }
  };
};

export const fetchProdsImg = (prodsInfo, prodsCategory) => {
  return async (dispatch) => {
    const prodsId = prodsCategory.reduce(
      (prodsAllId, category) => [
        ...prodsAllId,
        ...prodsInfo[category.name].map((prodInfo) => prodInfo.id),
      ],
      []
    );
    // Fetching image function return an object which is a list of images with regarding product id
    const fetchImg = async (allIds) => {
      const allImgs = await Promise.all(
        allIds.map(async (prodId) => {
          const eachProdImg = await listAll(ref(storage, `sanpham/${prodId}`));
          try {
            const prodImg = await Promise.all(
              eachProdImg.items.map(async (item) => {
                const url = await getDownloadURL(item);
                try {
                  return { name: item.name, url: url };
                } catch (err) {
                  throw new Error("Get image url failed!");
                }
              })
            );
            return { [prodId]: prodImg };
          } catch (error) {
            throw new Error("Fetch image failed!");
          }
        })
      );
      return allImgs;
    };
    try {
      const imgList = await fetchImg(prodsId);
      dispatch(productActions.setProdsImg(imgList));
    } catch (error) {
      throw new Error("Fetch image failed!");
    }
  };
};
