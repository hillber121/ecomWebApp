import { createSlice } from "@reduxjs/toolkit";

const initialProductState = {
  quantity: 1,
  ogProdsInfo: null,
  displayedProdsInfo: null,
  ogCategory: null,
  displayedCategory: null,
  priceFilter: null,
  prodsImg: null,
  categoryTotalLen: 0,
  currentOpt: "",
  prevProdsInfo: null,
};

const ProductSlice = createSlice({
  name: "Product",
  initialState: initialProductState,
  reducers: {
    // Set products' information, category, category length, payload: products data
    setData(state, action) {
      state.ogProdsInfo = action.payload.productsInfo;
      state.displayedProdsInfo = Object.values(
        action.payload.productsInfo
      ).flat();
      state.ogCategory = action.payload.category;
      state.displayedCategory = action.payload.category;
      state.categoryTotalLen = action.payload.category.length;
      state.priceFilter = action.payload.filter;
    },
    // Set category, payload: filtered category
    setCategory(state, action) {
      // Remove filter
      state.prevProdsInfo = null;
      state.displayedProdsInfo = state.ogProdsInfo;
      state.currentOpt = "";

      if (action.payload !== "all") {
        state.displayedCategory = [
          state.ogCategory.find((category) => category.name === action.payload),
        ];
      } else {
        state.displayedCategory = state.ogCategory;
      }
      state.displayedProdsInfo = state.displayedCategory
        .map((category) => state.ogProdsInfo[category.name])
        .flat();
    },
    // Set products image
    setProdsImg(state, action) {
      state.prodsImg = action.payload;
    },
    // Price filter , payload: {filter type, filter option id}
    setFilterPrice(state, action) {
      if (action.payload) {
        const { filterType, optId } = action.payload;
        const priceFilterHandler = (condition) => {
          // If products information has already be filtered, use previous products information instead
          return (state.prevProdsInfo || state.displayedProdsInfo).filter(
            (prodInfo) => {
              // Remove dot in price (string) and convert it to number
              const priceNumb = +prodInfo.prices[0].current.split(".").join("");
              return condition(priceNumb);
            }
          );
        };
        const priceSortHandler = (type) => {
          // If products information has already be filtered, use previous products information instead
          return [...(state.prevProdsInfo || state.displayedProdsInfo)].sort(
            (prodInfoA, prodInfoB) => {
              // Remove dot in price (string) and convert it to number
              const priceNumbA = +prodInfoA.prices[0].current
                .split(".")
                .join("");
              const priceNumbB = +prodInfoB.prices[0].current
                .split(".")
                .join("");
              if (type === "most") {
                return priceNumbA - priceNumbB;
              } else return priceNumbB - priceNumbA;
            }
          );
        };
        state.currentOpt = optId;
        let filteredProdsValue = null;
        if ("priceRange" === filterType) {
          if ("under500" === optId) {
            filteredProdsValue = priceFilterHandler((price) => price < 500000);
          }
          if ("from500to1000" === optId) {
            filteredProdsValue = priceFilterHandler(
              (price) => price >= 500000 && price <= 1000000
            );
          }
          if ("above1000" === optId) {
            filteredProdsValue = priceFilterHandler((price) => price > 1000000);
          }
        }
        if ("priceOrder" === filterType) {
          filteredProdsValue = priceSortHandler(optId);
        }

        // Reserve products information before filtering
        if (!state.prevProdsInfo) {
          state.prevProdsInfo = [...state.displayedProdsInfo];
        }

        state.displayedProdsInfo = filteredProdsValue;
      } else {
        state.displayedProdsInfo = [...state.prevProdsInfo];
        state.prevProdsInfo = null;
        state.currentOpt = "";
      }
    },
    // // Sort products price
    // setSortPrice(state, action) {
    //   const priceSortHandler = (type) => {
    //     // If products information has already be filtered, use previous products information instead
    //     return [
    //       ...(state.prevPriceSortedProdsInfo || state.displayedProdsInfo),
    //     ].sort((prodInfoA, prodInfoB) => {
    //       // Remove dot in price (string) and convert it to number
    //       const priceNumbA = +prodInfoA.prices[0].current.split(".").join("");
    //       const priceNumbB = +prodInfoB.prices[0].current.split(".").join("");
    //       if (type === "most") {
    //         return priceNumbA - priceNumbB;
    //       } else return priceNumbB - priceNumbA;
    //     });
    //   };
    //   if (action.payload) {
    //     state.currentSortedOpt = action.payload;
    //     const filteredProdsValue = priceSortHandler(action.payload);
    //     // Reserve products information before filtering
    //     if (!state.prevPriceSortedProdsInfo) {
    //       state.prevPriceSortedProdsInfo = [...state.displayedProdsInfo];
    //     }

    //     state.displayedProdsInfo = filteredProdsValue;
    //     // console.log(JSON.parse(JSON.stringify(state.prevPriceSortedProdsInfo)));
    //   } else {
    //     if (!state.prevPriceFilteredProdsInfo) {
    //       // If price filtering is not activate, deselect price sorting will set it to previous value
    //       state.displayedProdsInfo = [...state.prevPriceSortedProdsInfo];
    //     }
    //     state.prevPriceSortedProdsInfo = null;
    //     state.currentSortedOpt = "";
    //   }
    // },
  },
});

export const productActions = ProductSlice.actions;
export default ProductSlice.reducer;
