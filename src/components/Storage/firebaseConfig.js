// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAHBNWcxkzx6Q1q_3RKtSMtF1CYnqXiCMY",
  authDomain: "nhabong-6b68c.firebaseapp.com",
  projectId: "nhabong-6b68c",
  storageBucket: "nhabong-6b68c.appspot.com",
  messagingSenderId: "1095908946595",
  appId: "1:1095908946595:web:d279059e122878ea1cc516",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const storage = getStorage(app);
