import { Link } from "react-router-dom";

import "./_footer.scss";
import Input from "../UI/Input/Input";
import logoImg from "../../assets/images/logo.svg";
import {
  FacebookIcon,
  InstagramIcon,
  GoogleIcon,
  TwitterIcon,
  SendIcon,
  MastercardIcon,
  VisaIcon,
} from "./../SvgFile/SvgFile";
import { showToast } from "../UI/Helper/ToastMessage";

export default function Footer() {
  // Send email input attributes
  const sendEmailInpAtts = {
    id: "sendEmailInp",
    type: "email",
    placeholder: "Email của bạn là gì?",
  };
  const sendEmailInpIcon = {
    comp: <SendIcon />,
    submit: true,
  };
  // Send email handle
  const sendEmailHandler = (event) => {
    event.preventDefault();
    showToast();
  };

  // Contact handle
  const linkClicked = () => {
    showToast("Đang cập nhật liên kết");
  };

  return (
    <>
      <div className="large-margin-top" />
      <footer className="Footer">
        <div className="grid">
          <div className="Footer-container row">
            <div className="Footer__content Footer__info">
              <div className="Footer__content-title">
                <img src={logoImg} alt="logo" />
              </div>
              <ul className="Footer__content-main">
                <li>
                  Địa chỉ: Tòa nhà Tara Residence, phường 6, quận 8, <br />
                  thành phố Hồ Chí Minh
                </li>
                <li>Phone: +84909872305</li>
                <li>Email: hillber121@gmail.com</li>
                <li>
                  <ul className="Footer__info-contacts">
                    <li onClick={linkClicked}>
                      <FacebookIcon />
                    </li>
                    <li onClick={linkClicked}>
                      <GoogleIcon />
                    </li>
                    <li onClick={linkClicked}>
                      <InstagramIcon />
                    </li>
                    <li onClick={linkClicked}>
                      <TwitterIcon />
                    </li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className="Footer__content Footer__nav">
              <div className="Footer__content-title">Công ty</div>
              <ul className="Footer__content-main Footer__nav-links">
                <li>
                  <Link to="dich-vu">Dịch vụ</Link>
                </li>
                <li>
                  <Link to="gioi-thieu">Giới thiệu</Link>
                </li>
                <li>
                  <Link to="ho-tro">Hỗ trợ</Link>
                </li>
              </ul>
            </div>
            <div className="Footer__content Footer__news">
              <div className="Footer__content-title">Tin tức</div>
              <p className="Footer__news-desc">
                Nhận thông báo khi có sản phẩm mới cùng hàng ngàn ưu đãi
              </p>
              <form onSubmit={sendEmailHandler}>
                <Input
                  inp={sendEmailInpAtts}
                  inpIcon={sendEmailInpIcon}
                  className="Footer__news-form-inp"
                />
              </form>
            </div>
          </div>
          <div className="Footer-container row">
            <div className="Footer__other">
              <span>Copyright © 2022 Nhà Bông</span>
              <div className="Footer__other-payment">
                <VisaIcon />
                <MastercardIcon />
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  );
}
