import { Routes, Route } from "react-router-dom";

import HomePage from "../../pages/HomePage/HomePage";
import ServicesNavPage from "../../pages/ServicesPage/ServicesNavPage";
import SupportsNavPage from "../../pages/SupportsPage/SupportsNavPage";
import AboutPage from "../../pages/AboutPage/AboutPage";
import StorePage from "../../pages/StorePage/StorePage";
import ProdsPage from "../../pages/ProdsPage/ProdsPage";
import NotFoundPage from "../../pages/NotFoundPage/NotFoundPage";

function Main() {
  return (
    <div className="Main">
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="dich-vu/*" element={<ServicesNavPage />} />
        <Route path="ho-tro/*" element={<SupportsNavPage />} />
        <Route path="gioi-thieu" element={<AboutPage />} />
        <Route path="cua-hang" element={<StorePage />} />
        <Route path="san-pham/*" element={<ProdsPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </div>
  );
}

export default Main;
