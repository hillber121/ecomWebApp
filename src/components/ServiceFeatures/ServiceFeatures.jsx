import { Link } from "react-router-dom";

import "./_serviceFeatures.scss";
import {
  CoinIcon,
  WarrantyIcon,
  GiftIcon,
  PlaneIcon,
} from "./../SvgFile/SvgFile";

export default function ServiceFeatures() {
  return (
    <ul className="ServiceFeatures row">
      <li>
        <Link to="dich-vu/giao-hang">
          <PlaneIcon />
          <p>GIAO HÀNG</p>
        </Link>
      </li>
      <li>
        <Link to="dich-vu/goi-qua">
          <GiftIcon />
          <p>GÓI QUÀ</p>
        </Link>
      </li>
      <li>
        <Link to="dich-vu/hoan-tien">
          <CoinIcon />
          <p>HOÀN TIỀN</p>
        </Link>
      </li>
      <li>
        <Link to="dich-vu/bao-hanh">
          <WarrantyIcon />
          <p>BẢO HÀNH</p>
        </Link>
      </li>
    </ul>
  );
}
