import { useSelector, useDispatch } from "react-redux";

import "./_filter.scss";
import { productActions } from "../Store/product";

export default function Filter() {
  const dispatch = useDispatch();
  const { ogCategory, priceFilter, currentOpt } = useSelector(
    (state) => state.product
  );
  // Filter option component
  const filterHandle = (priceFilterData, optId) => {
    dispatch(
      productActions.setFilterPrice({
        filterType: priceFilterData.name,
        optId: optId,
      })
    );
  };

  const removeFilter = (event) => {
    event.stopPropagation();
    console.log(1);
    dispatch(productActions.setFilterPrice(null));
  };

  const optsComponent = priceFilter.map((priceFilterData) => (
    <div key={priceFilterData.name} className="Filter__group">
      <div className="Filter__group-title">{priceFilterData.title}</div>
      {priceFilterData.opts.map((opt) => {
        const filterOptClasses = `Filter__group-option ${
          currentOpt === opt.id
            ? "Filter__group-option--active"
            : "Filter__group-option--inactive"
        }`;

        return (
          <div
            key={opt.id}
            className={filterOptClasses}
            onClick={() => filterHandle(priceFilterData, opt.id)}
          >
            <span>{opt.text}</span>
            {currentOpt === opt.id && (
              <div
                className="Filter__group-option-close"
                onClick={(event) => removeFilter(event, priceFilterData)}
              />
            )}
          </div>
        );
      })}
    </div>
  ));

  return (
    <div className="Filter">
      <div className="Filter__group">
        <div
          className="Filter__group-item"
          onClick={() => {
            dispatch(productActions.setCategory("all"));
          }}
        >
          Tất cả sản phẩm
        </div>
        {ogCategory.map((category) => (
          <div
            key={category.name}
            className="Filter__group-item"
            onClick={() => {
              dispatch(productActions.setCategory(category.name));
            }}
          >
            {category.title}
          </div>
        ))}
      </div>
      {optsComponent}
    </div>
  );
}
