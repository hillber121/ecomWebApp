import { useMemo, useRef } from "react";
// core version + navigation, pagination modules:
import { Swiper, SwiperSlide } from "swiper/react";
import { Navigation, Autoplay } from "swiper";

import "./_slideShow.scss";
import slideImg1 from "./../../assets/images/slide_1.png";
import slideImg2 from "./../../assets/images/slide_2.png";
import { ArrowLIcon, ArrowRIcon } from "./../SvgFile/SvgFile";
// Import Swiper styles
import "swiper/scss";
import "swiper/scss/pagination";

export default function SlideShow() {
  // Carousel configuration
  const prevRef = useRef(null);
  const nextRef = useRef(null);
  const swiper = useMemo(
    () => ({
      containerModifierClass: "SlideShow__slider-",
      modules: [Navigation, Autoplay],
      onInit: (swiper) => {
        swiper.params.navigation.prevEl = prevRef.current;
        swiper.params.navigation.nextEl = nextRef.current;
        swiper.navigation.init();
        swiper.navigation.update();
      },
      navigation: {
        // Both prevEl & nextEl are null at render so this does not work
        prevEl: prevRef.current,
        nextEl: nextRef.current,
      },
      slidesPerView: 1,
      loop: true,
      autoplay: { delay: 5000 },
      speed: 400,
      className: "SlideShow-swiper",
    }),
    []
  );
  return (
    <div className="SlideShow row">
      <Swiper {...swiper}>
        {/* Project 1 */}
        <SwiperSlide className="SlideShow-tab">
          <img src={slideImg1} alt="hình 1" />
        </SwiperSlide>
        {/* Project 2 */}
        <SwiperSlide className="SlideShow-tab">
          <img src={slideImg2} alt="hình 2" />
        </SwiperSlide>
        <div ref={prevRef} className="SlideShow-btn--prev">
          <ArrowLIcon />
        </div>
        <div ref={nextRef} className="SlideShow-btn--nxt">
          <ArrowRIcon />
        </div>
      </Swiper>
    </div>
  );
}
