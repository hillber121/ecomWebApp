import { useEffect } from "react";
import { createPortal } from "react-dom";

import "./_modal.scss";

function Backdrop({ onClick, children }) {
  return (
    <div className="backdrop" onClick={onClick}>
      {children}
    </div>
  );
}

function ModalOverlay({ children }) {
  const modalClickHandler = (event) => {
    event.stopPropagation();
  };
  return (
    <div className="modal" onClick={modalClickHandler}>
      {children}
    </div>
  );
}

const overlays = document.getElementById("overlays");

export default function Modal(props) {
  // Disable scroll when modal is rendered
  useEffect(() => {
    document.body.style.overflow = "hidden";
    return () => {
      document.body.style.overflow = "initial";
    }
  }, []);

  return (
    <>
      {createPortal(
        <Backdrop onClick={props.onBackdropClick}>
          <ModalOverlay>{props.children}</ModalOverlay>
        </Backdrop>,
        overlays
      )}
    </>
  );
}
