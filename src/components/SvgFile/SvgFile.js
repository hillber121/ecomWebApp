import { ReactComponent as SearchIcon } from "./../../assets/icons/search.svg";
import { ReactComponent as HeartIcon } from "./../../assets/icons/heart.svg";
import { ReactComponent as CartIcon } from "./../../assets/icons/cart.svg";
import { ReactComponent as UserIcon } from "./../../assets/icons/user.svg";
import { ReactComponent as ArrowLIcon } from "./../../assets/icons/arrow_left.svg";
import { ReactComponent as ArrowRIcon } from "./../../assets/icons/arrow_right.svg";
import { ReactComponent as ArrowUIcon } from "./../../assets/icons/arrow_up.svg";
import { ReactComponent as ArrowDIcon } from "./../../assets/icons/arrow_down.svg";
import { ReactComponent as EyeOffIcon } from "./../../assets/icons/eye_off.svg";
import { ReactComponent as EyeOnIcon } from "./../../assets/icons/eye_on.svg";
import { ReactComponent as SendIcon } from "./../../assets/icons/send.svg";
import { ReactComponent as CloseIcon } from "./../../assets/icons/close.svg";
import { ReactComponent as PlusIcon } from "./../../assets/icons/plus.svg";
import { ReactComponent as MinusIcon } from "./../../assets/icons/minus.svg";
import { ReactComponent as CoinIcon } from "./../../assets/icons/service_features/coin.svg";
import { ReactComponent as WarrantyIcon } from "./../../assets/icons/service_features/warranty.svg";
import { ReactComponent as GiftIcon } from "./../../assets/icons/service_features/gift.svg";
import { ReactComponent as PlaneIcon } from "./../../assets/icons/service_features/plane.svg";
import { ReactComponent as FacebookIcon } from "./../../assets/icons/social_media/facebook.svg";
import { ReactComponent as InstagramIcon } from "./../../assets/icons/social_media/instagram.svg";
import { ReactComponent as GoogleIcon } from "./../../assets/icons/social_media/google.svg";
import { ReactComponent as TwitterIcon } from "./../../assets/icons/social_media/twitter.svg";
import { ReactComponent as MastercardIcon } from "./../../assets/icons/payment/mastercard.svg";
import { ReactComponent as VisaIcon } from "./../../assets/icons/payment/visa.svg";
import { ReactComponent as FacebookShareIcon } from "./../../assets/icons/share/facebook.svg";
import { ReactComponent as MessengerShareIcon } from "./../../assets/icons/share/messenger.svg";
import { ReactComponent as TelegramShareIcon } from "./../../assets/icons/share/telegram.svg";
import { ReactComponent as TwitterShareIcon } from "./../../assets/icons/share/twitter.svg";

export {
  SearchIcon,
  HeartIcon,
  CartIcon,
  UserIcon,
  ArrowLIcon,
  ArrowRIcon,
  ArrowUIcon,
  ArrowDIcon,
  EyeOffIcon,
  EyeOnIcon,
  CoinIcon,
  WarrantyIcon,
  GiftIcon,
  PlaneIcon,
  FacebookIcon,
  InstagramIcon,
  GoogleIcon,
  TwitterIcon,
  SendIcon,
  MastercardIcon,
  VisaIcon,
  CloseIcon,
  FacebookShareIcon,
  MessengerShareIcon,
  TelegramShareIcon,
  TwitterShareIcon,
  PlusIcon,
  MinusIcon,
};
