import { useEffect, useRef } from "react";

import "./_input.scss";

export default function Input(props) {
  // format:
  // props = {
  //     isFocus: {<boolean>},
  //     inpAtts: {
  //         id: <string>,
  //         type: <string>,
  //         placeholder: <string>,
  //         },
  //         inpTitle: <string>,
  //     inpIcon: {
  //         comp:<IconComponent />,
  //         submit: <boolean>, "optional"
  //         },
  //     validate: {
  //         required: <boolean>, "optional"
  //         email: <boolean>, "optional"
  //         password: <boolean>, "optional"
  //         blackSpace: <boolean>, "optional"
  //     } "optional"
  // }

  // Input id
  const inpId = props.inp.id;
  // Focus handle
  const inpRef = useRef();
  useEffect(() => {
    if (props.isFocus) {
      inpRef.current.focus();
    }
  }, [props.isFocus]);

  const btnRef = useRef();

  return (
    <div className={`Input ${props.className || ''}`}>
      {props.inpTitle && (
        <label htmlFor={inpId} className="Input__label-title">
          {props.inpTitle}
        </label>
      )}
      {props.inpIcon && (
        <button
          type={props.inpIcon.submit ? "submit" : "button"}
          className="Input__label-icon"
          ref={btnRef}
          onClick={!props.inpIcon.submit ? props.onClick : null}
        >
          {props.inpIcon.comp}
        </button>
      )}
      <div className="Input__inp"><input
        ref={inpRef}
        {...props.inp}
        className="Input__inp-field"
        placeholder={props.inp.placeholder}
      /></div>
    </div>
  );
}
