import { useMemo } from "react";
import { GoogleMap, useLoadScript, MarkerF } from "@react-google-maps/api";

import "./_myGoogleMap.scss";
import Loading from "../Loading/Loading";

export default function MyGoogleMap() {
  const { isLoaded } = useLoadScript({
    googleMapsApiKey: process.env.REACT_APP_PUBLIC_GOOGLE_MAPS_API_KEY,
  });
  if (!isLoaded) return <Loading loadingText="Đang tải bản đồ..." />;
  return <Map />;
}

function Map() {
  const center = useMemo(
    () => ({ lat: 10.732542858635284, lng: 106.65336111305301 }),
    []
  );

  return (
    <GoogleMap
      zoom={18}
      center={center}
      mapContainerClassName="MyGoogleMap__map"
    >
      <MarkerF position={center} visible={true} />
    </GoogleMap>
  );
}
