import "./_button.scss";

export default function Button(props) {
  let btnTypeClass = "Button__secondary";
  if (props.borderType) {
    btnTypeClass = "Button__border";
  }
  if (props.errorType) {
    btnTypeClass = "Button__error";
  }
  const btnClasses = `Button ${props.className || ""}
  ${btnTypeClass}`;
  
  return (
    <button
      type={props.type || "button"}
      className={btnClasses}
      onClick={props.onClick}
    >
      {props.children}
    </button>
  );
}
