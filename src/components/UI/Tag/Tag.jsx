import "./_tag.scss";

export default function Tag({ children }) {
  return <div className="Tag row">{children}</div>;
}
