import { Link, Routes, Route } from "react-router-dom";

import "./_articleNav.scss";
import Tag from "../Tag/Tag";

export default function ArticleNav(props) {
  const articleNavComponent = (
    <>
      <p className="ArticleNav__title">{props.title}</p>
      <ul className="ArticleNav__nav">
        {props.nav.map((item) => (
          <li key={item.path}>
            <Link to={item.path}>{item.desc}</Link>
          </li>
        ))}
      </ul>
    </>
  );
  return (
    <div className="ArticleNav">
      <Tag>{props.tag}</Tag>
      <Routes>
        <Route index element={articleNavComponent} />
        {props.nav.map((item) => (
          <Route key={item.path} path={item.path} element={item.pageComp} />
        ))}
      </Routes>
    </div>
  );
}
