import Feedback from "../../Feedback/Feedback";
import "./_article.scss";

export default function Article(props) {
  return (
    <div className="Article">
      <div className="Article__title">{props.title}</div>
      {props.content &&
        props.content.map((item, itemIndex) => (
          <div key={itemIndex} className="Article__content">
            {item.title && (
              <div className="Article__content-title">{item.title}</div>
            )}
            {item.desc && (
              <div className="Article__content-desc">
                {item.desc.map((itemDesc, itemDescIndex) => (
                  <div key={itemDescIndex}>
                    {itemDesc.main &&
                      itemDesc.main
                        .split("\n")
                        .map((text, textIndex) => (
                          <p key={textIndex}>{text}</p>
                        ))}
                    {itemDesc.list && (
                      <ol>
                        {itemDesc.list.map((listItem, listItemIndex) => (
                          <li key={listItemIndex}>{listItem.desc}</li>
                        ))}
                      </ol>
                    )}
                  </div>
                ))}
              </div>
            )}
            {item.img && (
              <img
                src={item.img.src}
                alt={item.img.alt}
                className="Article__content-img"
              />
            )}
          </div>
        ))}
      <Feedback />
    </div>
  );
}
