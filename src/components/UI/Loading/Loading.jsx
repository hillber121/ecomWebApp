import './_loading.scss';
import Modal from "../../Modal/Modal";
import loadingImg from "./../../../assets/images/loading.png";

export default function Loading({loadingText}) {
  return (
    <Modal>
      <div className="Loading">
        <img src={loadingImg} alt="hình loading" className='Loading__img' />
        <div className='Loading__text'>{loadingText}</div>
      </div>
    </Modal>
  );
}
