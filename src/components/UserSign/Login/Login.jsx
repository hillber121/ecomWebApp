import { useState } from "react";
import { useDispatch } from "react-redux";

import Input from "../../UI/Input/Input";
import { ModalActions } from "../../Store/modal";
import Button from "../../UI/Button/Button";
import LoginImg from "./../../../assets/images/login.png";
import LogoImg from "./../../../assets/images/logo.svg";
import { EyeOffIcon, EyeOnIcon } from "./../../SvgFile/SvgFile";

export default function Login() {
  const submitLoginHandler = (event) => {
    event.preventDefault();
  };
  // Modal handle
  const dispatch = useDispatch();
  const switchToSignupHandler = () => {
    dispatch(ModalActions.toSignup());
  };
  // User name input attributes
  const usernameInpAtts = {
    id: "usernameInp",
    type: "text",
    placeholder: "nhabong/nhabong@gmail.com",
  };
  const usernameInpTitle = "Tên đăng nhập/Email";
  // Pass input attributes
  const [isPassShowed, setIsPassShowed] = useState(false);
  const passInpAtts = {
    id: "passInp",
    type: isPassShowed ? "text" : "password",
    placeholder: "Tối thiểu 6 ký tự",
  };
  const passInpIcon = {
    comp: isPassShowed ? <EyeOnIcon /> : <EyeOffIcon />,
    submit: false,
  };
  const passInpTitle = "Mật khẩu";
  const togglePassHandler = () => {
    setIsPassShowed((prev) => !prev);
  };

  return (
    <>
      <img src={LoginImg} alt="login place" />
      <div className="User-container">
        <div className="User__logo">
          <img src={LogoImg} alt="logo" />
        </div>
        <div className="User__title">Đăng nhập</div>
        <form onSubmit={submitLoginHandler} className="User__form">
          <div className="User__form-username">
            <Input inp={usernameInpAtts} inpTitle={usernameInpTitle} />
          </div>
          <div className="User__form-pass">
            <Input
              inp={passInpAtts}
              inpTitle={passInpTitle}
              inpIcon={passInpIcon}
              onClick={togglePassHandler}
            />
            <div className="User__form-pass-forgot">Quên mật khẩu?</div>
          </div>
          <Button type="submit" className="User__form-submit">
            Đăng nhập
          </Button>
        </form>
        <div className="User__new" onClick={switchToSignupHandler}>
          Bạn là người mới? Đăng ký
        </div>
      </div>
    </>
  );
}
