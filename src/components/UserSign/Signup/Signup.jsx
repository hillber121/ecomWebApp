import { useState } from "react";

import Input from "../../UI/Input/Input";
import Button from "../../UI/Button/Button";
import signupImg from "./../../../assets/images/signup.png";
import logoImg from "./../../../assets/images/logo.svg";
import { EyeOffIcon, EyeOnIcon } from "./../../SvgFile/SvgFile";

export default function Signup() {
  const submitLoginHandler = (event) => {
    event.preventDefault();
  };
  // User name input attributes
  const usernameInpAtts = {
    id: "usernameInp",
    type: "text",
    placeholder: "nhabong",
  };
  const usernameInpTitle = "Tên đăng nhập";
  // User name input attributes
  const userEmailInpAtts = {
    id: "userEmailInp",
    type: "text",
    placeholder: "nhabong@gmail.com",
  };
  const userEmailInpTitle = "Email";
  // Pass input attributes
  const [isPassShowed, setIsPassShowed] = useState(false);
  const passInpAtts = {
    id: "passInp",
    type: isPassShowed ? "text" : "password",
    placeholder: "Tối thiểu 6 ký tự",
  };
  const passInpIcon = {
    comp: isPassShowed ? <EyeOnIcon /> : <EyeOffIcon />,
    submit: false,
  };
  const passInpTitle = "Mật khẩu";
  const togglePassHandler = () => {
    setIsPassShowed((prev) => !prev);
  };
  // Re-enter pass input attributes
  const [isRepassShowed, setIsRepassShowed] = useState(false);
  const repassInpAtts = {
    id: "RepassInp",
    type: isRepassShowed ? "text" : "password",
    placeholder: "Tối thiểu 6 ký tự",
  };
  const repassInpIcon = {
    comp: isRepassShowed ? <EyeOnIcon /> : <EyeOffIcon />,
    submit: false,
  };
  const repassInpTitle = "Nhập lại mật khẩu";
  const toggleRepassHandler = () => {
    setIsRepassShowed((prev) => !prev);
  };

  return (
    <>
      <img src={signupImg} alt="login place" />
      <div className="User-container">
        <div className="User__logo">
          <img src={logoImg} alt="logo" />
        </div>
        <div className="User__title">Đăng ký</div>
        <form onSubmit={submitLoginHandler} className="User__form">
          <div className="User__form-username">
            <Input inp={usernameInpAtts} inpTitle={usernameInpTitle} />
          </div>
          <div className="User__form-userEmail">
            <Input inp={userEmailInpAtts} inpTitle={userEmailInpTitle} />
          </div>
          <div className="User__form-pass">
            <Input
              inp={passInpAtts}
              inpTitle={passInpTitle}
              inpIcon={passInpIcon}
              onClick={togglePassHandler}
            />
          </div>
          <div className="User__form-repass">
            <Input
              inp={repassInpAtts}
              inpTitle={repassInpTitle}
              inpIcon={repassInpIcon}
              onClick={toggleRepassHandler}
            />
          </div>
          <Button type="submit" className="User__form-submit">
            Đăng ký
          </Button>
        </form>
      </div>
    </>
  );
}
