import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import "./_userSign.scss";
import Signup from "./Signup/Signup";
import Login from "./Login/Login";
import { ModalActions } from "../Store/modal";
import { CloseIcon } from "../SvgFile/SvgFile";

export default function UserSign() {
  const dispatch = useDispatch();
  const { loginState } = useSelector((state) => state.modal);
  // First time open modal will display Login form
  useEffect(() => {
    dispatch(ModalActions.toLogin());
  }, [dispatch]);
  // Close modal handle
  const closeModalHandler = () => {
    dispatch(ModalActions.closeModal());
  };

  return (
    <div className="User">
      {loginState ? <Login /> : <Signup />}
      <div className="User__close" onClick={closeModalHandler}>
        <CloseIcon />
      </div>
    </div>
  );
}
