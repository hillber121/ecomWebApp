import "./_feedback.scss";
import Button from "../UI/Button/Button";
import { showToast } from "../UI/Helper/ToastMessage";

export default function Feedback() {
  return (
    <div className="Feedback">
      <div className="Feedback__title">Bài viết có hữu ích cho bạn không?</div>
      <div className="Feedback__answers">
        <Button
          className="Feedback__answers-btn"
          borderType
          onClick={() => showToast()}
        >
          Có
        </Button>
        <Button
          className="Feedback__answers-btn"
          borderType
          onClick={() => showToast()}
        >
          Không
        </Button>
      </div>
    </div>
  );
}
