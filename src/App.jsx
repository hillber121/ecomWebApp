import { useEffect } from "react";
import { useDispatch } from "react-redux";

import "./App.scss";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Main from "./components/Main/Main";
import { fetchProductData } from "./components/Store/product-database";
import ToastMessage from "./components/UI/Helper/ToastMessage";

export default function App() {
  // First render will fetch products data
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(fetchProductData());
  }, [dispatch]);

  return (
    <>
      <ToastMessage />
      <Header />
      <div className="App grid">
        <Main />
      </div>
      <Footer />
    </>
  );
}
