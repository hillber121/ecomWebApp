import { useState, useEffect, useCallback, lazy, Suspense } from "react";
import { Routes, Route } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";

import "./_prodsPage.scss";
import Tag from "../../components/UI/Tag/Tag";
import Filter from "../../components/Filter/Filter";
import AllProds from "./AllProds";
import Loading from "../../components/UI/Loading/Loading";
import {
  fetchProdsImg,
  fetchProductData,
} from "../../components/Store/product-database";

const ProdDetailPage = lazy(() => import("./ProdDetailPage"));

export default function ProdsPage() {
  // Refresh will fetch products data again
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchProdsDataAgain = () => {
      dispatch(fetchProductData());
    };
    window.addEventListener("beforeunload", fetchProdsDataAgain);
    return () => {
      window.removeEventListener("beforeunload", fetchProdsDataAgain);
    };
  }, [dispatch]);

  const {
    displayedCategory,
    categoryTotalLen,
    displayedProdsInfo,
    ogProdsInfo,
    ogCategory,
  } = useSelector((state) => state.product);

  // First render will fetch products image
  useEffect(() => {
    if (!ogProdsInfo || !ogCategory) return;
    dispatch(fetchProdsImg(ogProdsInfo, ogCategory));
  }, [dispatch, ogProdsInfo, ogCategory]);

  // Tag title display
  const [tagTitle, setTagTitle] = useState("Tất cả sản phẩm");
  useEffect(() => {
    if (displayedCategory && categoryTotalLen && displayedProdsInfo) {
      if (displayedCategory.length === categoryTotalLen) {
        setTagTitle("Tất cả sản phẩm");
      } else {
        setTagTitle(displayedCategory[0].title);
      }
    }
  }, [displayedCategory, categoryTotalLen, displayedProdsInfo]);

  // Detail product pages component rendering
  const detailProdCompRender = useCallback((prodsInfo) => {
    return prodsInfo.map((prodInfo) => (
      <Route
        key={prodInfo.id}
        path={prodInfo.id}
        element={
          <Suspense fallback={<Loading loadingText="Đang tải dữ liệu..." />}>
            <ProdDetailPage prodInfo={prodInfo} />
          </Suspense>
        }
      />
    ));
  }, []);

  return (
    displayedCategory &&
    categoryTotalLen &&
    displayedProdsInfo && (
      <Routes>
        <Route
          index
          element={
            <div className="ProdsPage">
              <Tag>{tagTitle}</Tag>
              <div className="ProdsPage-container">
                <Filter />
                <AllProds />
              </div>
            </div>
          }
        />
        {detailProdCompRender(displayedProdsInfo)}
      </Routes>
    )
  );
}
