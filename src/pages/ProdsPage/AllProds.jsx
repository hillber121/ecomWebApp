import { useState, useEffect, useCallback } from "react";
import { useSelector } from "react-redux";

import "./_allProds.scss";
import Product from "../../components/Product/Product";

export default function AllProds() {
  const { displayedProdsInfo, displayedCategory, prodsImg } =
    useSelector((state) => state.product);
  // Count the number of products
  const [prodsAmount, setProdsAmount] = useState(0);
  useEffect(() => {
    let amount = 0;
    amount = displayedProdsInfo.length;

    setProdsAmount(amount);
  }, [displayedCategory, displayedProdsInfo]);

  //Handle products rendered
  const prodCompRender = useCallback(
    (prodsInfo) => {
      return prodsInfo.map((prodInfo) => {
        return (
          prodsImg && (
            <Product
              key={prodInfo.id}
              className="Product--inpage"
              prodPath={prodInfo.id}
              prodId={prodInfo.id}
              prodTitle={prodInfo.title}
              prodPrices={prodInfo.prices}
              prodImg={prodsImg.find((prodImg) => prodImg[prodInfo.id])}
            />
          )
        );
      });
    },
    [prodsImg]
  );
  const prodsComp = prodCompRender(displayedProdsInfo);

  return (
    <div className="AllProds">
      <p className="AllProds__amount">{prodsAmount} sản phẩm</p>
      <div className="AllProds__prods">{prodsComp}</div>
    </div>
  );
}
