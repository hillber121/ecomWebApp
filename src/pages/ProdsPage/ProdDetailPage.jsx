import { useState, useEffect } from "react";
import { useSelector } from "react-redux";

import "./_prodDetailPage.scss";
import accomServicesImg from "./../../assets/images/slide_2.png";
import {
  FacebookShareIcon,
  MessengerShareIcon,
  TelegramShareIcon,
  TwitterShareIcon,
  PlusIcon,
  MinusIcon,
} from "./../../components/SvgFile/SvgFile";
import Button from "../../components/UI/Button/Button";
import { showToast } from "../../components/UI/Helper/ToastMessage";

// define list of images

export default function ProdDetailPage({ prodInfo }) {
  const { title, prices, description, detail, id } = prodInfo;
  const { prodsImg } = useSelector((state) => state.product);

  // Image view handle
  const [mainImg, setMainImg] = useState(null);
  const [imgList, setImgList] = useState(null);
  useEffect(() => {
    if (!prodsImg) {
      return;
    }
    const prodsImgObj = prodsImg.reduce((acc, val) => {
      Object.assign(acc, val);
      return acc;
    }, {});
    setImgList(prodsImgObj[id]);
    setMainImg(prodsImgObj[id][0]);
  }, [prodsImg, id]);

  // Like button clicked handle
  const likeClicked = () => {
    showToast();
  };

  // Size selected handle
  const [selectedPrice, setSelectedPrice] = useState(prices[0]);

  const sizeSelectHandler = (event) => {
    if (selectedPrice.size) {
      const selectedSize = event.target.innerText;
      setSelectedPrice(prices.find((price) => price.size === selectedSize));
    }
  };

  // Size options component
  const sizesComp = (
    <ul className="ProdDetailPage__main-sizes">
      {selectedPrice.size &&
        prices.map((price) => {
          const sizeActiveClass =
            price.size === selectedPrice.size
              ? "ProdDetailPage__main-sizes-opt--active"
              : " ";
          return (
            <li
              key={price.size}
              className={sizeActiveClass}
              onClick={sizeSelectHandler}
            >
              {price.size}
            </li>
          );
        })}
    </ul>
  );

  // Discount implementation
  const [discount, setDiscount] = useState(0);
  useEffect(() => {
    const convertToNumb = (price) => {
      const splittedPrice = price.split(".");
      if (splittedPrice.length === 2) {
        return +splittedPrice.slice(0, 1).join("");
      }
      return +splittedPrice.slice(0, 2).join("");
    };
    const currentPrice = convertToNumb(selectedPrice.current);
    const oldPrice = convertToNumb(selectedPrice.old);
    const discountVal = 100 - Math.floor((currentPrice / oldPrice) * 100);
    setDiscount(discountVal);
  }, [selectedPrice]);

  // Quantity update handle
  const [prodQuantity, setProdQuantity] = useState(1);
  const deQuantity = () => {
    if (prodQuantity <= 1) {
      return;
    }
    setProdQuantity((prevQuantity) => --prevQuantity);
  };

  const inQuantity = () => {
    if (prodQuantity >= 99) {
      return;
    }
    setProdQuantity((prevQuantity) => ++prevQuantity);
  };

  // Click to action handle
  const addToCartClicked = () => {
    showToast();
  };
  const buyNowClicked = () => {
    showToast();
  };

  // Share on social media handle
  const socialMediaIcons = [
    { id: "twitter", hrefLink: "#", icon: <TwitterShareIcon /> },
    { id: "facebook", hrefLink: "#", icon: <FacebookShareIcon /> },
    { id: "messenger", hrefLink: "#", icon: <MessengerShareIcon /> },
    { id: "telegram", hrefLink: "#", icon: <TelegramShareIcon /> },
  ];
  const shareComp = (
    <div className="ProdDetailPage__main-share">
      <span className="ProdDetailPage__main-share-title">Chia sẻ:</span>
      {socialMediaIcons.map((socialMediaIcon) => (
        <a
          key={socialMediaIcon.id}
          href={socialMediaIcon.hrefLink}
          className="ProdDetailPage__main-share-opt"
        >
          {socialMediaIcon.icon}
        </a>
      ))}
    </div>
  );

  // ProdDetailPage__others-nav handle
  const [isInfo, setIsInfo] = useState(true);
  const infoClasses = `ProdDetailPage__others-nav-btn ${
    isInfo ? "ProdDetailPage__others-nav-btn--active" : ""
  }`;
  const accomServicesClasses = `ProdDetailPage__others-nav-btn ${
    !isInfo ? "ProdDetailPage__others-nav-btn--active" : ""
  }`;

  const infoComp = (
    <div className="ProdDetailPage__others-info">
      <div className="ProdDetailPage__others-info-text">{detail}</div>
      <div className="ProdDetailPage__others-info-imgs">
        {imgList &&
          imgList.map((img) => (
            <img key={img.name} src={img.url} alt={img.name} />
          ))}
      </div>
    </div>
  );

  const accomServicesComp = (
    <div className="ProdDetailPage__others-services">
      <img src={accomServicesImg} alt="accompanying servies" />
    </div>
  );

  return (
    <div className="ProdDetailPage">
      <div className="small-margin-top" />
      <div className="ProdDetailPage__main">
        <div className="ProdDetailPage__main-imgs">
          {mainImg && (
            <img
              src={mainImg.url}
              alt={mainImg.name}
              className="ProdDetailPage__main-imgs-main"
            />
          )}
          {imgList &&
            imgList.map((img) => {
              const selectMainImg = (event) => {
                const selectedImgSrc = event.target.getAttribute("src");
                setMainImg(imgList.find((img) => img.url === selectedImgSrc));
              };
              return (
                <img
                  key={img.name}
                  src={img.url}
                  alt={img.name}
                  onClick={selectMainImg}
                />
              );
            })}
        </div>
        <div className="ProdDetailPage__main-content">
          <div className="ProdDetailPage__main-title">{title}</div>
          <div className="ProdDetailPage__main-like" onClick={likeClicked}>
            <div className="ProdDetailPage__main-like-icon" />
          </div>
          {selectedPrice && (
            <div className="ProdDetailPage__main-prices">
              <div className="ProdDetailPage__main-prices--current">
                {selectedPrice.current}
              </div>
              <div className="ProdDetailPage__main-prices--old">
                {selectedPrice.old}
              </div>
              <div className="ProdDetailPage__main-prices-discount">
                {discount && discount}%
              </div>
            </div>
          )}
          <div className="ProdDetailPage__main-desc">{description}</div>
          {sizesComp}
          <div className="ProdDetailPage__main-quantity">
            <span className="ProdDetailPage__main-quantity-title">
              Số lượng
            </span>
            <div className="ProdDetailPage__main-quantity-action">
              <MinusIcon
                className="ProdDetailPage__main-quantity-action-icon"
                onClick={deQuantity}
                onMouseDown={(event) => event.preventDefault()}
              />
              <div className="ProdDetailPage__main-quantity-action-numb">
                {prodQuantity < 10 ? `0${prodQuantity}` : prodQuantity}
              </div>
              <PlusIcon
                className="ProdDetailPage__main-quantity-action-icon"
                onClick={inQuantity}
                onMouseDown={(event) => event.preventDefault()}
              />
            </div>
          </div>
          <div className="ProdDetailPage__main-cta">
            <Button
              className="ProdDetailPage__main-cta-btn"
              borderType
              onClick={addToCartClicked}
            >
              Thêm vào giỏ hàng
            </Button>
            <Button
              className="ProdDetailPage__main-cta-btn"
              onClick={buyNowClicked}
            >
              Mua ngay
            </Button>
          </div>
          {shareComp}
        </div>
      </div>
      <div className="small-margin-top" />
      <div className="ProdDetailPage__others">
        <div className="ProdDetailPage__others-nav">
          <span className={infoClasses} onClick={() => setIsInfo(true)}>
            Thông tin sản phẩm
          </span>
          <span
            className={accomServicesClasses}
            onClick={() => setIsInfo(false)}
          >
            Dịch vụ đi kèm
          </span>
        </div>
        {isInfo ? infoComp : accomServicesComp}
      </div>
    </div>
  );
}
