import Tag from "../../components/UI/Tag/Tag";
import MyGoogleMap from "../../components/UI/MyGoogleMap/MyGoogleMap";

export default function StorePage() {
  return (
    <div className="StorePage">
      <Tag>Địa điểm cửa hàng Nhà Bông</Tag>
      <MyGoogleMap />
    </div>
  );
}
