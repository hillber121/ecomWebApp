import { Link } from "react-router-dom";

import "./_notFoundPage.scss";
import notFoundImg from "./../../assets/images/notfound.svg";
import Button from "../../components/UI/Button/Button";

export default function NotFoundPage() {
  return (
    <div className="NotFoundPage">
      <div className="small-margin-top" />
      <img src={notFoundImg} alt="not found" />
      <p>Tính năng đang được cập nhật</p>
      <Link to="/">
        <Button className="NotFoundPage__btn" errorType>
          Trở về trang chủ
        </Button>
      </Link>
    </div>
  );
}
