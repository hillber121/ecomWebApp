import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";

import "./_homePage.scss";
import ServiceFeatures from "../../components/ServiceFeatures/ServiceFeatures";
import SlideShow from "../../components/SlideShow/SlideShow";
import Prods from "../../components/Prods/Prods";
import { fetchProdsImg } from "../../components/Store/product-database";

export default function HomePage() {
  const { ogProdsInfo, ogCategory } = useSelector((state) => state.product);

  // First render will fetch products image
  const dispatch = useDispatch();
  useEffect(() => {
    if (!ogProdsInfo || !ogCategory) return;
    dispatch(fetchProdsImg(ogProdsInfo, ogCategory));
  }, [dispatch, ogProdsInfo, ogCategory]);

  return (
    <div className="HomePage">
      <SlideShow />
      <ServiceFeatures />
      {ogProdsInfo &&
        ogCategory &&
        ogCategory.map((category) => {
          return (
            <Prods
              key={category.name}
              category={category}
              ogInfo={ogProdsInfo}
            />
          );
        })}
    </div>
  );
}
