import { lazy, Suspense } from "react";

import Loading from "../../components/UI/Loading/Loading";
const Article = lazy(() => import("../../components/UI/Article/Article"));

export default function DeliveryMethodPage() {
  const deliveryMethodPageProps = {
    title: "Các câu hỏi về Phương thức vận chuyển",
    content: [
      {
        title: "1. Các Phương thức vận chuyển",
        desc: [
          {
            main: "Hiện tại, người mua có thể chọn 1 trong 3 phương thức vận chuyển mới: Hỏa tốc - Nhanh - Tiết kiệm khi mua sản phẩm, thay vì phải lựa chọn một đơn vị vận chuyển cố định.",
          },
        ],
      },
      {
        title:
          "2. Người mua có thể thay đổi phương thức vận chuyển của đơn hàng không?",
        desc: [
          {
            main: "Rất tiếc, người mua không thể thay đổi tùy chọn phương thức vận chuyển sau khi đơn hàng đã được giao cho đơn vị vận chuyển.",
          },
        ],
      },
    ],
  };
  return (
    <Suspense fallback={<Loading loadingText="Đang tải dữ liệu..." />}>
      <Article {...deliveryMethodPageProps} />
    </Suspense>
  );
}
