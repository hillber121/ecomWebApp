import { lazy, Suspense } from "react";

import Loading from "../../components/UI/Loading/Loading";
const Article = lazy(() => import("../../components/UI/Article/Article"));

export default function RefundTimePage() {
  const refundTimePageProps = {
    title: "Mất bao lâu để được hoàn tiền sau khi gửi trả hàng thành công?",
    content: [
      {
        title: "Nếu Nhà Bông xác nhận hoàn tiền",
        desc: [
          {
            main: "Tiền hoàn sẽ được gửi tài khoản Tín dụng/Ghi nợ người mua cung cấp. Thời gian nhận được tiền hoàn từ 7 đến 14 ngày làm việc (tùy theo ngân hàng).",
          },
        ],
      },
      {
        title: "⚠️ Lưu ý",
        desc: [
          {
            list: [
              {
                desc: "Phí trả hàng sẽ được hỗ trợ hoàn lại cho bạn theo Chính sách hỗ trợ phí trả hàng.",
              },
              {
                desc: "Nhà Bông chỉ hỗ trợ hoàn tiền về đúng tài khoản Tín dụng/Ghi nợ người mua cung cấp.",
              },
            ],
          },
        ],
      },
    ],
  };
  return (
    <Suspense fallback={<Loading loadingText="Đang tải dữ liệu..." />}>
      <Article {...refundTimePageProps} />
    </Suspense>
  );
}
