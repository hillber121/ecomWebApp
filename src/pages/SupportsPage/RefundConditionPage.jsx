import { lazy, Suspense } from "react";

import Loading from "../../components/UI/Loading/Loading";
const Article = lazy(() => import("../../components/UI/Article/Article"));

export default function RefundConditionPage() {
  const RefundConditionPageProps = {
    title: "Điều kiện Trả hàng/Hoàn tiền của Nhà Bông",
    content: [
      {
        title: "1. Thời gian để gửi yêu cầu trả hàng/hoàn tiền",
        desc: [
          {
            main: "7 ngày kể từ lúc đơn hàng được cập nhật trạng thái Giao hàng thành công.",
          },
        ],
      },
      {
        title: "2. Lý do trả hàng/hoàn tiền",
        desc: [
          {
            list: [
              {
                desc: "Hàng nhận được bị thiếu/khác mô tả/hư hỏng.",
              },
              { desc: "Chưa nhận được hàng sau thời gian giao hàng dự kiến." },
            ],
          },
        ],
      },
      {
        title: "3. Bằng chứng cần cung cấp",
        desc: [
          {
            main: "Người mua cần cung cấp hình ảnh và/hoặc video thể hiện rõ tình trạng sản phẩm nhận được.\nNhà Bông có thể yêu cầu bổ sung bằng chứng nếu bằng chứng cung cấp bị mờ, nhòe, thiếu, không thể hiện được tình trạng sản phẩm nhận được.",
          },
        ],
      },
      {
        title: "4. Tình trạng của hàng trả lại ",
        desc: [
          {
            main: "Sau khi đã gửi yêu cầu Trả hàng/Hoàn tiền, người mua cần gửi trả hàng về Nhà Bông theo hướng dẫn qua email.\nĐể hạn chế các rắc rối phát sinh liên quan đến trả hàng, lưu ý:",
            list: [
              {
                desc: "Đóng gói cẩn thận.",
              },
              {
                desc: "Gửi trả toàn bộ sản phẩm, bao gồm tất cả phụ kiện đi kèm, hóa đơn, tem phiếu bảo hành (nếu có).",
              },
              {
                desc: "Sản phẩm gửi trả phải trong tình trạng như khi nhận hàng.",
              },
            ],
          },
          {
            main: "Nhà Bông khuyến khích người mua chuẩn bị thêm các bằng chứng sau để làm bằng chứng đối chiếu/khiếu nại về sau nếu cần:",
            list: [
              {
                desc: "Bằng chứng giao/nhận hàng với đầy đủ các thông tin: đơn vị vận chuyển, mã vận đơn, tên Người gửi/Người nhận, số điện thoại liên lạc và địa chỉ giao hàng.",
              },
              { desc: "Video clip quay lại quá trình đóng gói hàng gửi trả." },
            ],
          },
        ],
      },
    ],
  };
  return (
    <Suspense fallback={<Loading loadingText="Đang tải dữ liệu..." />}>
      <Article {...RefundConditionPageProps} />
    </Suspense>
  );
}
