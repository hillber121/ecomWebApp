import ArticleNav from "../../components/UI/Article/ArticleNav";
import DeliveryMethodPage from "./DeliveryMethodPage";
import RefundConditionPage from "./RefundConditionPage";
import RefundTimePage from "./RefundTimePage";

export default function SupportsNavPage() {
  const supportsNavPageProps = {
    title: "Câu hỏi thường gặp",
    tag: "Trung tâm hỗ trợ",
    nav: [
      {
        path: "mat-bao-lau-de-duoc-hoan-tien",
        desc: "Mất bao lâu để được hoàn tiền sau khi gửi trả hàng thành công?",
        pageComp: <RefundTimePage />,
      },
      {
        path: "dieu-kien-tra-hang-hoan-tien",
        desc: "Điều kiện Trả hàng/Hoàn tiền của Nhà Bông",
        pageComp: <RefundConditionPage />,
      },
      {
        path: "phuong-thuc-van-chuyen",
        desc: "Phương thức vận chuyển",
        pageComp: <DeliveryMethodPage />,
      },
    ],
  };
  return <ArticleNav {...supportsNavPageProps} />;
}
