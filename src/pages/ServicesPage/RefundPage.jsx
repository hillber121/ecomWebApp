import { lazy, Suspense } from "react";

import Loading from "../../components/UI/Loading/Loading";
const Article = lazy(() => import("../../components/UI/Article/Article"));

export default function RefundPage() {
  const refundPageProps = {
    title: "Chính sách trả hàng và hoàn tiền",
    content: [
      {
        title: "1. Điều kiện áp dụng",
        desc: [
          {
            main: "Theo các điều khoản và điều kiện được quy định trong Chính sách Trả hàng và Hoàn tiền này và tạo thành một phần của Điều khoản dịch vụ, Nhà Bông đảm bảo quyền lợi của người mua bằng cách cho phép gửi yêu cầu hoàn trả sản phẩm và/hoặc hoàn tiền trước khi hết hạn. Thời hạn được quy định trong Điều khoản Dịch vụ.\nNhà Bông hỗ trợ Người dùng trong việc giải quyết các xung đột có thể phát sinh trong quá trình giao dịch.",
          },
        ],
      },
      {
        title: "2. Điều kiện yêu cầu trả hàng/hoàn tiền",
        desc: [
          {
            main: "Người mua đồng ý rằng cô ấy/anh ấy chỉ có thể yêu cầu trả hàng/hoàn tiền trong các trường hợp sau:",
            list: [
              {
                desc: "Người mua đã thanh toán nhưng không nhận được sản phẩm.",
              },
              {
                desc: "Sản phẩm bị lỗi hoặc bị hư hại trong quá trình vận chuyển.",
              },
              { desc: "Sản phẩm sai kích cỡ, sai màu sắc, ...." },
              {
                desc: "Sản phẩm bị trả lại không phải là sản phẩm hạn chế trả hàng.",
              },
            ],
          },
          {
            main: "Nhà Bông luôn xem xét cẩn thận các yêu cầu trả hàng/hoàn tiền của Người mua và có quyền đưa ra quyết định cuối cùng đối với yêu cầu đó dựa trên các quy định nêu trên và theo Điều khoản Dịch vụ của Nhà Bông.",
          },
        ],
      },
      {
        title: "3. Không thay đổi ý định mua hàng",
        desc: [
          {
            main: "Trừ khi được đề cập trong Chính sách Trả hàng và Hoàn tiền này, các trường hợp trả hàng do người mua thay đổi ý định mua hàng sẽ không được chấp nhận.",
          },
        ],
      },
      {
        title: "4. Tình trạng của hàng trả lại",
        desc: [
          {
            main: "Để hạn chế các rắc rối phát sinh liên quan đến trả hàng, người mua lưu ý cần phải đóng gói cẩn thận và gửi trả sản phẩm bao gồm toàn bộ phụ kiện đi kèm, hóa đơn VAT, tem phiếu bảo hành (nếu có) và sản phẩm phải trong tình trạng nguyên vẹn như khi nhận hàng. Người mua bắt buộc phải quay video hoặc chụp lại ảnh sản phẩm ngay khi nhận được và trong lúc đóng gói hàng trả về để làm bằng chứng đối chiếu/khiếu nại về sau.",
          },
        ],
      },
      {
        title: "5. Hoàn tiền cho hàng trả lại",
        desc: [
          {
            main: "Nhà Bông chỉ hoàn tiền cho người mua khi người mua đáp ứng một trong các trường hợp sau:",
            list: [
              { desc: "Xác nhận hàng đã được trả lại." },
              {
                desc: "Đơn hàng của người mua đủ điều kiện được trả hàng/hoàn tiền và Nhà Bông quyết định cho Người mua được hoàn tiền khi người mua đã gửi trả hàng.",
              },
            ],
          },
          {
            main: "Tiền hoàn trả sẽ được chuyển vào thẻ Tín dụng/Ghi nợ người mua cung cấp.",
          },
        ],
      },
    ],
  };

  return (
    <Suspense fallback={<Loading loadingText="Đang tải dữ liệu..." />}>
      <Article {...refundPageProps} />
    </Suspense>
  );
}
