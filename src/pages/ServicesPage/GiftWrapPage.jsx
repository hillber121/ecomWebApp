import { lazy, Suspense } from "react";

import Loading from "../../components/UI/Loading/Loading";
import giftWrapImg from "./../../assets/images/service/giftwrap.png";
const Article = lazy(() => import("../../components/UI/Article/Article"));

export default function GiftWrapPage() {
  const giftWrapPageProps = {
    title: "Gói quà siêu xinh",
    content: [
      {
        img: {
          src: giftWrapImg,
          alt: "gift wrap info graphic",
        },
      },
    ],
  };
  return (
    <Suspense fallback={
      <Loading loadingText="Đang tải dữ liệu..."/>}>
      <Article {...giftWrapPageProps} />
    </Suspense>
  );
}
