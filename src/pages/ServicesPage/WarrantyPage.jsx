import { lazy, Suspense } from "react";

import Loading from "../../components/UI/Loading/Loading";
import warrantyImg from "./../../assets/images/service/warranty.png";
const Article = lazy(() => import("../../components/UI/Article/Article"));

export default function WarrantyPage() {
  const warrantyPageProps = {
    title: "Chính sách bảo hành",
    content: [
      {
        title: "Các dịch vụ mua hàng chuyên nghiệp",
        desc: [
          {
            list: [
              {
                desc: "Miễn phí gói quà và nén nhỏ gấu.",
              },
              { desc: "Tặng thiệp miễn phí khi mua hàng." },
              {
                desc: "Giao hàng trong nội thành tận tay người mua nhanh chóng chỉ trong vòng 60 phút.",
              },
              {
                desc: "Thời gian giao hàng trên toàn quốc chỉ từ 2 - 5 ngày và áp dụng hình thức ship COD linh hoạt, thanh toán tiền khi nhận hàng.",
              },
              {
                desc: "Các sản phẩm gấu tại Nhà Bông được bảo hành miễn phí 1 năm khi gấu bị bục chỉ hoặc xẹp bông.",
              },
              { desc: "Dịch vụ giặt gấu và khử khuẩn gấu tại nhà giá rẻ." },
            ],
          },
        ],
        img: {
          src: warrantyImg,
          alt: "warranty info graphic",
        },
      },
    ],
  };
  return (
    <Suspense fallback={<Loading loadingText="Đang tải dữ liệu..." />}>
      <Article {...warrantyPageProps} />
    </Suspense>
  );
}
