import ArticleNav from "../../components/UI/Article/ArticleNav";
import DeliveryPage from "./DeliveryPage";
import GiftWrapPage from "./GiftWrapPage";
import RefundPage from "./RefundPage";
import WarrantyPage from "./WarrantyPage";

export default function ServicesNavPage() {
  const servicesNavPageProps = {
    title: "Các dịch vụ độc quyền tại Nhà Bông",
    tag: "Các dịch vụ tại Nhà Bông",
    nav: [
      {
        path: "giao-hang",
        desc: "Giao hàng",
        pageComp: <DeliveryPage />,
      },
      {
        path: "goi-qua",
        desc: "Gói quà",
        pageComp: <GiftWrapPage />,
      },
      {
        path: "hoan-tien",
        desc: "Hoàn tiền",
        pageComp: <RefundPage />,
      },
      {
        path: "bao-hanh",
        desc: "Bảo hành",
        pageComp: <WarrantyPage />,
      },
    ],
  };

  return (
    <>
      <ArticleNav {...servicesNavPageProps} />
    </>
  );
}
