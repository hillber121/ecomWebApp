import { lazy, Suspense } from "react";
import Loading from "../../components/UI/Loading/Loading";

const Article = lazy(() => import("../../components/UI/Article/Article"));

export default function DeliveryPage() {
  const deliveryPageProps = {
    title: "Hướng dẫn đặt mua hàng/giao hàng",
    content: [
      {
        title: "1. Săn gấu",
        desc: [{ main: "Chọn sản phẩm yêu thích của bạn trên website." }],
      },
      {
        title: "2. Tư vấn/Đặt hàng",
        desc: [
          {
            main: "Chat zalo hoặc gọi điện trực tiếp: 0909872305 để được tư vấn thêm.\nĐặt hàng trực tiếp trên website. Kiểm tra sản phẩm, màu sắc, kích thước và tổng.\nNgười mua cần cung cấp tên, số điện thoại và địa chỉ nhận hàng.\nSau khi đặt hàng thành công, Nhà Bông sẽ gọi điện xác nhận đơn hàng.",
          },
        ],
      },
      {
        title: "3. Thanh toán",
        desc: [
          {
            main: "2 hình thức thanh toán:",
            list: [
              { desc: "Giao hàng tận nhà thu COD." },
              { desc: "Chuyển khoản." },
            ],
          },
        ],
      },
      {
        title: "4. Giao hàng",
        desc: [
          {
            main: "Giao siêu tốc trong nội thành Tp.Hồ Chí Minh.\nGửi chuyển phát nhanh đến tận nhà đối với các đơn hàng tỉnh, nhận sau 2-4 ngày.",
          },
        ],
      },
    ],
  };

  return (
    <Suspense fallback={<Loading loadingText="Đang tải dữ liệu..." />}>
      <Article {...deliveryPageProps} />
    </Suspense>
  );
}
