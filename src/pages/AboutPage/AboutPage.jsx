import './_aboutPage.scss';
import Tag from "../../components/UI/Tag/Tag";

export default function AboutPage() {
  return (
    <div className="AboutPage">
      <Tag>Giới thiệu về Nhà Bông</Tag>
      <div className="AboutPage__content">
        <p>
          <b>Sứ mệnh:</b> nắm rõ nhu cầu cải thiện giá trị tinh thần, Nhà Bông
          khởi đầu hành trình vào năm 2022 với mong muốn làm cầu nối giữa người
          với người. Gấu bông, hoa sáp là những món quà vô cùng dễ thương và ý
          nghĩa để trao tặng cho bạn bè, người thân. Bên cạnh đó, gấu bông còn
          là người bạn mỗi tối của rất nhiều người.
        </p>
        <p>
          <b>Sản phẩm:</b> Nhà Bông luôn cam kết, đảm bảo chất lượng sản phẩm để
          người mua có được trải nghiệm êm ái nhất. Tất cả các sản phẩm tại Nhà
          Bông đều có nguồn gốc xuất xứ rõ ràng, chất lượng, đa dạng chủng loại
          với giá hợp lý.
        </p>
        <p>
          <b>Nhân sự:</b> Đội ngũ nhân viên chuyên nghiệp, tâm huyết và là niềm
          tự hào của Nhà Bông. Vì vậy, đời sống tinh thần của nhân viên cũng
          được quan tâm, chăm sóc cùng những chính sách đãi ngộ, nâng cao tinh
          thần làm việc và truyền cảm hứng để nhân viên cống hiến và mang lại
          những giá trị cho bản thân và cộng đồng.
        </p>
      </div>
    </div>
  );
}
